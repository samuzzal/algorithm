'''Solve some interesting word problems.'''

import unittest

class Solution(object):
    def __init__(self, sentence):
        '''Initiate the Solution.'''
        self.sentence = sentence
        self.word_map = {}

        # populate the word map where word is the key and a list of indices
        # where that occurs is the value
        for index in range(len(sentence)):
            if sentence[index] not in self.word_map:
                self.word_map[sentence[index]] = [index]
            else:
                self.word_map[sentence[index]].append(index)

    def find_word_distance(self, word1, word2):
        '''Measure the distance of two words in a sentence.'''
        if word1 not in self.word_map:
            return None
        if word2 not in self.word_map:
            return None
        if word1 == word2:
            return 0

        minm = len(self.sentence)
        for w1_index in self.word_map[word1]:
            for w2_index in self.word_map[word2]:
                diff = abs(w1_index - w2_index)
                if diff < minm:
                    minm = diff
        return minm
            
        
class Tester(unittest.TestCase):
    '''Unit test class to test the method of Solution.'''
    
    def test_init(self):
        '''Test the initilizer of Solution class.''' 
        total_tests = 3

        input = list()
        input.append(['there', 'is', 'a', 'blue', 'pen', 'on', 'a',
                'blue', 'table', 'with', 'blue', 'dots'])
        input.append([])
        input.append(['abc', 'abc', 'abc'])
        input.append([' ', '  ', '	'])

        expected_output = list()
        expected_output.append({
                            'a': [2, 6],
                            'blue': [3, 7, 10],
                            'on': [5],
                            'is': [1],
                            'there': [0],
                            'dots': [11],
                            'pen': [4],
                            'table': [8],
                            'with': [9]
                          })
        expected_output.append({})
        expected_output.append({'abc': [0,1,2]})
        expected_output.append({' ': [0], '  ': [1], '	': [2]})

        for testcase in range(total_tests):
            s = Solution(input[testcase])
            self.assertDictEqual(s.word_map, expected_output[testcase])

    
    def test_find_word_distance(self):
        '''Test the find_word_distance method of Solution.'''
        input = ['there', 'is', 'a', 'blue', 'pen', 'on', 'a',
                'blue', 'table', 'with', 'blue', 'dots']
        s = Solution(input)
        self.assertEqual(s.find_word_distance('there', 'there'), 0)
        self.assertEqual(s.find_word_distance('there', 'is'), 1)
        self.assertEqual(s.find_word_distance('a', 'blue'), 1)
        self.assertEqual(s.find_word_distance('blue', 'table'), 1)
        self.assertEqual(s.find_word_distance('there', 'dots'), 11)
        self.assertEqual(s.find_word_distance('abcd', 'there'), None)
        self.assertEqual(s.find_word_distance('blue', 'abcd'), None)

if __name__ == '__main__':
    unittest.main(verbosity=2)
