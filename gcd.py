states = [1,1,1,0,1,1,1,1]
days = 2

def cellCompete(states, days):
    # Let's iterate through the days
    output = [0] * len(states)
    for day in range(days):
        print(states)
        for index in range(len(states)):
            print('index', index)
            if index == 0:
                prev = 0
            else:
                prev = states[index-1]
            print('prev',prev)

            if index+1 == len(states):
                next = 0
            else:
                next = states[index+1]
            print('next', next)
            
            if prev == 1 and next == 1:
                output[index] = 0
            elif prev == 0 and next == 0:
                output[index] = 0
            else:
                output[index] = 1
            print("output[{}] = {}".format(index, output[index]))
        states = list(output)
    
    return output

cellCompete(states,2)



def generalizedGCD(num, arr):
    # Find the maximum number in the arr
    max = arr[0]
    for element in arr:
        if element > max:
            max = element

    for i in range(max, 0, -1):
        for j in range(len(arr)):
            if arr[j] % i:
                break
            if j+1 == len(arr):
                if not arr[j] % i:
                    return i
    return 1

print(generalizedGCD(5, [2,3,4,5,6]))
print(generalizedGCD(5, [2,4,6,8,10]))
