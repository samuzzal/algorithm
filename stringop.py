class Solution(object):
    def lengthOfLongestSubstring(self, s):
        """
        :type s: str
        :rtype: int
        """
        substrs = {}
        substr = "" 
        substr_len = 0
        for char in s:
            if char not in substr:
                substr += char
                substr_len += 1
                print (substr)
                substrs[substr.decode('utf8')] = substr_len
            else:
                if substrs[substr.decode('utf8')] < substr_len:
                    substrs.clear()
                    substrs[substr] = substr_len
                substr = char
                substr_len = 1
        
        return substrs[substr]
                
s = Solution()
print(s.lengthOfLongestSubstring('abcabcbb'))


def minimumTime(numOfSubFiles, files):
    sorted_file_sizes = list(files)
    total_time = 0
    for i in range(numOfSubFiles):
        print(sorted_file_sizes)
        print('i = ', i)
        if i == numOfSubFiles - 1:
            break

        # if there is none to be merged, return the only element left
        if not len(sorted_file_sizes):
            print('breaks 1')
            break
            
        if len(sorted_file_sizes) == 1:
            print('breaks 2')
            break
            
        # Sort the files' list so the first two elements can be 
        # merged in the lowest time possible
        sorted_file_sizes = sorted(sorted_file_sizes)
        print('sorted_file_sizes 1 ', sorted_file_sizes)
        sum_of_sizes = sorted_file_sizes[0] + sorted_file_sizes[1]
        total_time += sum_of_sizes
        print('total_time ', total_time)
        
        # Modify the sorted list by removing the merged sizes and then
        # by adding the sum of two sizes calculated earlier into that
        sorted_file_sizes.pop(0)
        print('sorted_file_sizes 2 ', sorted_file_sizes)
        sorted_file_sizes.pop(0)
        print('sorted_file_sizes 3 ', sorted_file_sizes)
        sorted_file_sizes.append(sum_of_sizes)
        print('sorted_file_sizes 4 ', sorted_file_sizes)
        
    return total_time

#print(minimumTime(4, [20, 4, 8, 2]))


def optimalUtilization(deviceCapacity, foregroundAppList, backgroundAppList):
    # Some verifications
    if not len(foregroundAppList):
        return [[]]
    if not len(backgroundAppList):
        return [[]]
    
    maxval = 0
    maxmap = {maxval: [[]]}
    for fa in foregroundAppList:
        for ba in backgroundAppList:
            # if the resource requirement is more than the deviceCapacity,
            # skip the combination
            if fa[1]+ba[1] > deviceCapacity:
                continue

            if fa[1]+ba[1] > maxval:
                maxmap.clear()
                maxval = fa[1]+ba[1]
                maxmap[maxval] = [[fa[0], ba[0]]]
            elif fa[1]+ba[1] == maxval:
                maxmap[maxval].append([fa[0], ba[0]])
    return maxmap[maxval]

deviceCapacity = 10
foregroundAppList = [[1,3], [2,5], [3,7], [4,10]]
backgroundAppList = [[1,2], [2,3], [3,4], [4,5]]
print(optimalUtilization(deviceCapacity, foregroundAppList, backgroundAppList))

deviceCapacity = 7
foregroundAppList = [[1,2],[2,4],[3,6]]
backgroundAppList = [[1,2]]
print(optimalUtilization(deviceCapacity, foregroundAppList, backgroundAppList))
